# The Open Celtic Dictionary API Server

[![pipeline status](https://gitlab.com/prvInSpace/open-celtic-dictionary-api-server/badges/master/pipeline.svg)](https://gitlab.com/prvInSpace/open-celtic-dictionary-api-server/-/commits/master)
[![coverage report](https://gitlab.com/prvInSpace/open-celtic-dictionary-api-server/badges/master/coverage.svg)](https://gitlab.com/prvInSpace/open-celtic-dictionary-api-server/-/commits/master)
[![License: MIT](https://img.shields.io/badge/License-MIT-blue.svg)](https://opensource.org/licenses/MIT)
[![Discord](https://img.shields.io/discord/718008955040956456.svg?label=&logo=discord&logoColor=ffffff&color=7389D8&labelColor=6A7EC2)](https://discord.gg/UzaFmfV)

This is the APi server for the [Open Celtic Dictionary project](https://digi.prv.cymru).
The goal of the project is to be able to provide speakers of Celtic languages a dictionary API with all words, conjugations, some mutations, translations, and other useful information.

It is entirely open source and free for anyone to use.

The API server is written in Kotlin.

## Community

Due to the history of the project, most of the developer community is located in the projects [Discord server](https://discord.gg/UzaFmfV).
If you got any questions, suggestions, you need any help, or if you just want to pop by to have a cup of coffee or tea, then feel free to join!
You are more than welcome to pop by!

## Notes

The dictionary doesn't contain all the words currently, but if given a word-type it will try to auto-generate the conjugations and other fields.

## Available dictionaries

These are the available dictionaries of the API.
These are full dictionaries.

| Language        | Code | Repository |
|-----------------|------|------------|
| Breton          | br   | [Link](https://gitlab.com/prvInSpace/open-breton-dictionary) |
| Irish           | ga   | [Link](https://gitlab.com/prvInSpace/open-irish-dictionary) |
| Scottish Gaelic | gd   | [Link](https://gitlab.com/prvInSpace/open-gaelic-dictionary) |
| Welsh           | cy   | [Link](https://gitlab.com/prvInSpace/open-welsh-dictionary) |

## Usage

The API is hosted on https://api.prv.cymru:13999/api/dictionary, but can also be cloned and self hosted.
The API returns the result as UTF-8 formatted JSON.

Note that there are several other end-points for different usages.

### Parameters for the main end-point

| Parameter | Description                                                                       | Default      |
|-----------|-----------------------------------------------------------------------------------|--------------|
| word      | The word that you want to conjugate.                                              | |
| id        | The ID of the word to search for.                                                 | |  
| type      | The type of the word you want to conjugate. Optional, but required for generation | |
| lang      | The language code of the dictionary you want to use.                              | cy           |
| search    | A flag that sets whether to search conjugated terms as well as the lookup term.   | False        |

NB: You need to pass either a word or an id.

#### Results

If an error is accounted with the provided parameters, the API will return a JSON object with the success set to 'false'.
The field error will contain a detailed error message with the issue encountered.

If the API succeeds (meaning that it were able to handle the request, this does not mean it will return a result) the API will
return a JSON object with the success flag set to 'true' and an array called 'results' that contains all the words it found.

* For extended information about the returned results please refer to the documentation for each language.

### Examples

* Example with the Welsh word 'gweld' (to see): https://api.prv.cymru:13999/api/dictionary?lang=cy&word=gweld&type=verb
* Usage of the API in the bot Digi: https://gitlab.com/prvInSpace/digi


## Building from the command line

Make sure you have Java 11 installed and the java command line set to the correct verion

On Arch-based distributions:

`sudo archlinux-java set java-11-openjdk`

On debian-based distributions:

`update-java-alternatives --list
sudo update-java-alternatives --set /path/to/java/version`

Then go in the project directory and run:

`./gradlew shadowJar`

You can now run the server by executing it (exact path of the jar file may vary):

`java -jar build/libs/Open\ Celtic\ Dictionary\ API\ Server-1.0.0-all.jar`

The api is now available on http://localhost:13999/api/dictionary.

## Contributing

If you have found a word that is conjugated wrongly or you have any other enquiry or bug report, please feel free to either leave an issue on this page or send me an email.
We always need help with expanding the dictionary!

## Special Thanks and Acknowledgements

I want to extend a special thanks to everyone who has contributed to the project since its inception.
Since the original repository has been split into several repositories I want to list the contributors to that
repository here in no particular order:

* Seán Breathnach
* Gwlanbzh
* Zander Urq.
* Gwenn M
* Issac Richards
* David Kong

## Maintainers

* Preben Vangberg &lt;prv@aber.ac.uk&gt;


