import org.jetbrains.kotlin.gradle.tasks.KotlinCompile
import com.github.jengelman.gradle.plugins.shadow.tasks.ShadowJar

plugins {
    kotlin("jvm") version "1.5.20"
    id("jacoco")
    id("de.jansauer.printcoverage") version "2.0.0"
    id("com.github.johnrengelman.shadow") version "5.2.0"
}

group = "cymru.prv"
version = "1.0.0"

tasks.withType<ShadowJar> {
    manifest {
        attributes["Main-Class"] = "cymru.prv.dictionary.api.OpenCelticApiServerKt"
    }
}

repositories {
    mavenCentral()
    maven(url = "https://gitlab.com/api/v4/projects/24450764/packages/maven")
    maven(url = "https://gitlab.com/api/v4/projects/24632737/packages/maven")
    maven(url = "https://gitlab.com/api/v4/projects/24653157/packages/maven")
    maven(url = "https://gitlab.com/api/v4/projects/24633977/packages/maven")
    maven(url = "https://gitlab.com/api/v4/projects/24623449/packages/maven")
    maven(url = "https://gitlab.com/api/v4/projects/25723650/packages/maven")
    maven(url = "https://gitlab.com/api/v4/projects/25744530/packages/maven")
    maven(url = "https://gitlab.com/api/v4/projects/27052082/packages/maven")
    maven(url = "https://gitlab.com/api/v4/projects/28798141/packages/maven")
    maven(url = "https://gitlab.com/api/v4/projects/28904146/packages/maven")
}

dependencies {
    implementation ( "org.http4k:http4k-core:4.7.0.0")
    implementation ( "org.http4k:http4k-server-apache:4.7.0.0")
    implementation ("cymru.prv:open-dictionary:1.0.+")
    implementation ("cymru.prv:open-cornish-dictionary:1.+")
    implementation ("cymru.prv:open-breton-dictionary:1.0.+")
    implementation ("cymru.prv:open-french-dictionary:1.+")
    implementation ("cymru.prv:open-german-dictionary:1.+")
    implementation ("cymru.prv:open-irish-dictionary:1.0.+")
    implementation ("cymru.prv:open-gaelic-dictionary:1.0.+")
    implementation ("cymru.prv:open-spanish-dictionary:1.+")
    implementation ("cymru.prv:open-welsh-dictionary:1.0.+")
    implementation ("cymru.prv:open-nynorsk-dictionary:1.0.+")
    testImplementation(kotlin("test-junit5"))
    testImplementation("org.junit.jupiter:junit-jupiter-api:5.6.0")
    testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine:5.6.0")
    implementation(kotlin("stdlib-jdk8"))
}

tasks.test {
    useJUnitPlatform()
    finalizedBy(tasks.jacocoTestReport)
}

tasks.jacocoTestReport {
    reports {
        xml.isEnabled = true
        xml.destination = file("${project.buildDir}/reports/jacoco/test/jacocoTestReport.xml")
        html.isEnabled = true
    }
    dependsOn(tasks.test)
    finalizedBy(tasks.printCoverage)
}

tasks.withType<KotlinCompile> {
    kotlinOptions.jvmTarget = "11"
}